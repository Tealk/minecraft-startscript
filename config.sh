#!/bin/bash
# version 6.0.0 (08.08.2016)
# Author:  tealk@anzahcraft.de
# http://anzahcraft.de/resources/linux-minecraftserver-startscript.2/

<<LICENCE
Minecraftserver Startscript for Linux von Anzahcraft ist lizenziert unter einer Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz.
Über diese Lizenz hinausgehende Erlaubnisse können Sie unter https://anzahcraft.de erhalten.
LICENCE

SERVERS="1" #number of servers
BKUP="/home/mine/backup" #backup directory
BACKUPOLD="true" #remove the old log files
CRONBACKUPOLD="true" #same for cron
BKUPOLDT="14" #log lifetime
CHKMYSQL="false" #check whether MySQL is online 
INTROOUTRO="true" #activate intro and outro


DIR1="/home/mine/server1" #directory to the server
SERVERSOFTWARE1="spigot" # spigot,craftbukkit (name of Java file)
PARAMS1="-Xms32m -Xmx5120M -XX:ParallelGCThreads=2" #parameter Xms initial and xmx maximum heap size
NAME1="server1" #porzess name
DESC1="server1 Server" #server name
BKUPWORLD1="true" #true= backup world
WORLDNAME1="world" #world name for the backup
BKUPNETHER1="false" #true= backup nether
CRONCOMMAND1="" #cron command for the server
SERVERUPDATE1="false" #true= activate server update
PLUGINUPDATE1="false" #Currently not functional
MODUPDATE1="false" #Currently not functional

DIR2="/home/mine/server2"
SERVERSOFTWARE2="spigot"
PARAMS2="-Xms32m -Xmx5120M -XX:ParallelGCThreads=2"
NAME2="server2"
DESC2="server2 Server"
BKUPWORLD2="false"
WORLDNAME2="world"
BKUPNETHER2="false"
CRONCOMMAND2=""
SERVERUPDATE2="false"
PLUGINUPDATE2="false" #Currently not functional
MODUPDATE2="false" #Currently not functional

DIR3="/home/mine/server3"
SERVERSOFTWARE3="spigot"
PARAMS3="-Xms32m -Xmx5120M -XX:ParallelGCThreads=2"
NAME3="server3"
DESC3="server3 Server"
BKUPWORLD3="false"
WORLDNAME3="world"
BKUPNETHER3="false"
CRONCOMMAND3=""
SERVERUPDATE3="false"
PLUGINUPDATE3="false" #Currently not functional
MODUPDATE3="false" #Currently not functional

DIR4="/home/mine/server4"
SERVERSOFTWARE4="spigot"
PARAMS4="-Xms32m -Xmx5120M -XX:ParallelGCThreads=2"
NAME4="server4"
DESC4="server4 Server"
BKUPWORLD4="false"
WORLDNAME4="world"
BKUPNETHER4="false"
CRONCOMMAND4=""
SERVERUPDATE4="false"
PLUGINUPDATE4="false" #Currently not functional
MODUPDATE4="false" #Currently not functional

DELETEPLUGIN="false"
DELETEWORLD="false"

_DEBUG="on"

#don't touch if you don't know what you do
BUILDBOX="/home/mine/Buildbox"

DAEMON="java"
MODJAR="$SERVERSOFTWARE.jar"
PARAMSEND="nogui"

SERVVAR1() {
	DIR="$DIR1"
	SERVERSOFTWARE="SERVERSOFTWARE1"
	PARAMS="$PARAMS1"
	NAME="$NAME1"
	DESC="$DESC1"
	BKUPWORLD="BKUPWORLD1"
	WORLDNAME="WORLDNAME1"
	BKUPNETHER="BKUPNETHER1"
	CRONCOMMAND="CRONCOMMAND1"
	SERVERUPDATE="SERVERUPDATE1"
	PLUGINUPDATE="PLUGINUPDATE1"
	MODUPDATE="MODUPDATE1"
}

SERVVAR2() {
	DIR="$DIR2"
	SERVERSOFTWARE="SERVERSOFTWARE2"
	PARAMS="$PARAMS2"
	NAME="$NAME2"
	DESC="$DESC2"
	BKUPWORLD="BKUPWORLD2"
	WORLDNAME="WORLDNAME2"
	BKUPNETHER="BKUPNETHER2"
	CRONCOMMAND="CRONCOMMAND2"
	SERVERUPDATE="SERVERUPDATE2"
	PLUGINUPDATE="PLUGINUPDATE2"
	MODUPDATE="MODUPDATE2"
}

SERVVAR3() {
	DIR="$DIR3"
	SERVERSOFTWARE="SERVERSOFTWARE3"
	PARAMS="$PARAMS3"
	NAME="$NAME3"
	DESC="$DESC3"
	BKUPWORLD="BKUPWORLD3"
	WORLDNAME="WORLDNAME3"
	BKUPNETHER="BKUPNETHER3"
	CRONCOMMAND="CRONCOMMAND3"
	SERVERUPDATE="SERVERUPDATE3"
	PLUGINUPDATE="PLUGINUPDATE3"
	MODUPDATE="MODUPDATE3"
}

SERVVAR4() {
	DIR="$DIR4"
	SERVERSOFTWARE="SERVERSOFTWARE4"
	PARAMS="$PARAMS4"
	NAME="$NAME4"
	DESC="$DESC4"
	BKUPWORLD="BKUPWORLD4"
	WORLDNAME="WORLDNAME4"
	BKUPNETHER="BKUPNETHER4"
	CRONCOMMAND="CRONCOMMAND4"
	SERVERUPDATE="SERVERUPDATE4"
	PLUGINUPDATE="PLUGINUPDATE4"
	MODUPDATE="MODUPDATE4"
}