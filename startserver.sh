#!/bin/bash
# version 6.0.0 (08.08.2016)
# Author:  tealk@anzahcraft.de
# http://anzahcraft.de/resources/linux-minecraftserver-startscript.2/

<<LICENCE
Minecraftserver Startscript for Linux von Anzahcraft ist lizenziert unter einer Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz.
Über diese Lizenz hinausgehende Erlaubnisse können Sie unter https://anzahcraft.de erhalten.
LICENCE


INPUT=/tmp/menu.sh.$$
OUTPUT=/tmp/output.sh.$$

DATE=$(date +%d.%m.%y-%H.%M)
dat=(date +"%A %d in %B of %Y (%r)")

h=(date +%H)
if [ $h -lt 12 ]; then
  GREETING="Good Morning $USER, Have a nice day!"
elif [ $h -lt 18 ]; then
  GREETING="Good Afternoon $USER"
else
  GREETING="Good Evening $USER"
fi

trap 'rm $OUTPUT; rm $INPUT; exit' SIGHUP SIGINT SIGTERM

function msg_output(){
	local h=${1-10}			# box height default 10
	local w=${2-41} 		# box width default 41
	local t=${3-Output} 	# box title
	dialog --backtitle "Minecraft Shell Script" --title "${t}" --clear --colors --msgbox "$(<$OUTPUT)" ${h} ${w}
}

function info_output(){
	local h=${1-10}			# box height default 10
	local w=${2-41} 		# box width default 41
	local t=${3-Output} 	# box title
	dialog --backtitle "Minecraft Shell Script" --title "${t}" --colors --infobox "$(<$OUTPUT)" ${h} ${w}
}

function yesno_output(){
	local h=${1-10}			# box height default 10
	local w=${2-41} 		# box width default 41
	local t=${3-Output} 	# box title
	dialog --backtitle "Minecraft Shell Script" --title "${t}" --colors --yesno "$(<$OUTPUT)" ${h} ${w}
}

function debug_on(){
	if [ "$_DEBUG" = "on" ]; then
		set -x
	fi
}

function STARTS() {
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
	if [ "$UID" = "0" ]; then
		printf "\Z1WARNING! \Z0For security reasons we advise: DO NOT RUN THE SERVER AS ROOT" >$OUTPUT
		msg_output 10 60 "START SERVER"
		QUIT
	fi
	if [ "$CHKMYSQL" = "true" ]; then
		if [[ ! `ps -ef | grep mysqld | grep -v root` = 0 ]]; then
			printf "MySQL seems to be \Z1not \Z0running check your MySQL server or \ndisable this option only in your config.sh" >$OUTPUT
			msg_output 10 60 "MYSQL SERVER"
			QUIT
		fi
	fi
	if [ -e "$DIR/mcserver.pid" ]; then
		PID=$(cat "$DIR/mcserver.pid")
		if ( kill -0 $PID 2> /dev/null ); then
			printf "%s seems to be online \nDont start the Server twice try restart or stop" "$DESC" >$OUTPUT
			msg_output 10 60 "STOP $DESC"
			MENU
		else
			printf "mcserver.pid found, but no server running. \nPossibly your previously started server crashed \nPlease view the logfile for details." >$OUTPUT
			msg_output 10 60 "STOP $DESC"
			rm "$DIR/mcserver.pid"
			MENU
		fi
	fi
	START
}

function STARTF() {
	if [ $ACTION != RESTART ] || [ $ACTION != RESTARTALL ]; then
		MENU
	fi
}

function STOPS() {
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
	if [ -e "$DIR/mcserver.pid" ]; then
		PID=$(cat "$DIR/mcserver.pid")
		if ( kill -0 $PID 2> /dev/null ); then
			STOP
		else
			printf "mcserver.pid found, but no server running. \nPossibly your previously started server crashed \nPlease view the logfile for details." >$OUTPUT
			msg_output 10 60 "STOP $DESC"
			rm "$DIR/mcserver.pid"
			MENU
		fi
	else
		printf "%s seems to be offline (mcserver.pid is missing)" "$DESC" >$OUTPUT
		msg_output 10 60 "STOP $DESC"
		MENU
	fi

}

function STOPF() {
	if [ $ACTION != RESTART ] || [ ACTION != $RESTARTALL ]; then
		MENU
	fi
}

function RESTART() {
	STOPS
	sleep 4
	STARTS
	exit 1
	MENU
}

function STATUS() {
	clear
	if [ "$SERVERS" -ge "1" ]; then
		SERVVAR1
		if [ -e "$DIR/mcserver.pid" ]; then
			if ( kill -0 $(cat "$DIR/mcserver.pid") 2> /dev/null ); then
				STATUSS1="$DESC -> \Z2online\Z0"
			else
				STATUSS1="$DESC -> \Z1offline\Z0"
			fi
		else
			STATUSS1="$DESC -> \Z1offline\Z0 (mcserver.pid missing)" >$OUTPUT
		fi
	fi
	if [ "$SERVERS" -ge "2" ]; then
		SERVVAR2
		if [ -e "$DIR/mcserver.pid" ]; then
			if ( kill -0 $(cat "$DIR/mcserver.pid") 2> /dev/null ); then
				STATUSS2="$DESC -> \Z2online\Z0"
			else
				STATUSS2="$DESC -> \Z1offline\Z0"
			fi
		else
			STATUSS2="$DESC -> \Z1offline\Z0 (mcserver.pid missing)" >$OUTPUT
		fi
	fi
	if [ "$SERVERS" -ge "3" ]; then
		SERVVAR3
		if [ -e "$DIR/mcserver.pid" ]; then
			if ( kill -0 $(cat "$DIR/mcserver.pid") 2> /dev/null ); then
				STATUSS3="$DESC -> \Z2online\Z0"
			else
				STATUSS3="$DESC -> \Z1offline\Z0"
			fi
		else
			STATUSS3="$DESC -> \Z1offline\Z0 (mcserver.pid missing)" >$OUTPUT
		fi
	fi
	if [ "$SERVERS" -ge "4" ]; then
		SERVVAR4
		if [ -e "$DIR/mcserver.pid" ]; then
			if ( kill -0 $(cat "$DIR/mcserver.pid") 2> /dev/null ); then
				STATUSS4="$DESC -> \Z2online\Z0"
			else
				STATUSS4="$DESC -> \Z1offline\Z0"
			fi
		else
			STATUSS4="$DESC -> \Z1offline\Z0 (mcserver.pid missing)" >$OUTPUT
		fi
	fi
#	printf "%s \n%s \n%s \n%s" "$STATUSS1" "$STATUSS2" "$STATUSS3" "$STATUSS4" >$OUTPUT
#	msg_output 10 60 "SERVERSTATUS"
	printf "%s \n%s \n%s \n%s \nBack to Menu?" "$STATUSS1" "$STATUSS2" "$STATUSS3" "$STATUSS4" >$OUTPUT
	yesno_output 10 60 "SERVERSTATUS"
	SERVEROFF=$?
	if [ "$SERVEROFF" = 0 ]; then
		MENU
	else
		QUIT
	fi
}

function SAY() {
	clear
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
	dialog --clear --cancel-label "Exit" --ok-label "Send" \
	--backtitle "Minecraft Server Startscript" \
	--title "SAY IN CHAT" \
	--inputbox "You are going to write on the Server:" 8 60 2>$OUTPUT
	respose=$?
	smessage=$(<"${OUTPUT}")
	case $respose in
	  0)
			if [ -e "$DIR/mcserver.pid" ]; then
				PID=$(cat "$DIR/mcserver.pid")
				if ( kill -0 $PID 2> /dev/null ); then
					screen -S "$NAME" -p 0 -X stuff "$(printf "say %s\r" "$smessage")"
					printf "You say on %s: \n%s" "$DESC" "$smessage" >$OUTPUT
					info_output 10 60 "SAY IN CHAT"
					sleep 3
				else
					printf "%s seems to be offline" "$DESC" >$OUTPUT
					msg_output 10 60 "SAY IN CHAT"
				fi
				MENU
			else
				printf "%s seems to be offline (mcserver.pid is missing)" "$DESC" >$OUTPUT
				msg_output 10 60 "STOP $DESC"
				MENU
			fi
		;;
		1)
			printf "Exit pressed, go back to main menu." >$OUTPUT
			info_output 10 60 "SAY IN CHAT"
			sleep 3
			MENU
		;;
		255)
			printf "[ESC] key pressed, go back to main menu." >$OUTPUT
			info_output 10 60 "SAY IN CHAT"
			sleep 3
			MENU
		;;
	esac
}

function LOGIN() {
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
	if [ -e "$DIR/mcserver.pid" ]; then
		PID=$(cat "$DIR/mcserver.pid")
		if ( kill -0 $PID 2> /dev/null ); then
			clear
			screen -r "$PID"
			QUIT
		else
			printf "%s seems to be offline" "$DESC" >$OUTPUT
			msg_output 10 60 "CONSOLE $DESC"
			MENU
		fi
	else
		printf "%s seems to be offline (mcserver.pid is missing)" "$DESC" >$OUTPUT
		msg_output 10 60 "STOP $DESC"
		MENU
	fi
exit 1
}

function RESTARTALL() {
		SERVER=1
		RESTART
		if [ "$SERVERS" -ge "2" ]; then
			SERVER=2
			sleep 1
			RESTART
		fi
		if [ "$SERVERS" -ge "3" ]; then
			SERVER=3
			sleep 1
			RESTART
		fi
		if [ "$SERVERS" -ge "4" ]; then
			SERVER=4
			sleep 1
			RESTART
		fi
	MENU
}

function UPDATES() {
	if [ "$FORGEUPDATE" = "false" ] && [ "$SPONGEGEUPDATE" = "false" ] && [ "$PLUGINUPDATE" = "false" ] && [ "$MODUPDATE" = "false" ]; then
		printf 'You must enable one Update option' >$OUTPUT
		msg_output 10 60 "UDPATE SERVER"
	else
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
		UPDATE
	fi
}

function UPDATEF() {
	if [ "$SERVERUP" = "success" ]; then
		printf "Clean up download folder?" >$OUTPUT
		yesno_output 10 60 "DELETE FILES"
		FILEDELETE=$?
		if [ "$FILEDELETE" = 0 ]; then
			rm  -rf $BUILDBOX
		fi
		printf "Server up tp date. You want a restart now?" >$OUTPUT
		yesno_output 10 60 "UDPATE SERVER"
		SERVERRESTART=$?
		if [ "$SERVERRESTART" = 0 ]; then
			RESTARTALL
		else
			printf "Please restart server!" >$OUTPUT
			info_output 10 60 "UDPATE SERVER"
			sleep 3
			MENU
		fi
	else
		printf "Server have no Updates, no restart necessary" >$OUTPUT
		info_output 10 60 "UDPATE SERVER"
		sleep 3
		MENU
	fi
}

function DELETEPLUGIN() {
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
	dialog --clear --cancel-label "Exit" --ok-label "Delete" \
	--backtitle "Minecraft Server Startscript" \
	--title "DELETE PLUGIN" \
	--inputbox "You are going to delete a Plugin, write worldname " 8 60 2>$OUTPUT
	respose=$?
	dlpl=$(<"${OUTPUT}")
	case $respose in
	  0)
		printf "Remove the plugin %s" "$dlpl?" >$OUTPUT
		yesno_output 10 60 "DELETE PLUGIN"
		FILEDELETE=$?
		if [ "$FILEDELETE" = 0 ]; then
			if [ -e "$DIR/plugins/$dlpl.jar" ]; then
				rm -rf "$DIR/mods/$dlpl.jar"
				printf "The plugin %s is deleted" "$dlpl" >$OUTPUT
				info_output 10 60 "DELETE PLUGIN"
				sleep 3
				MENU
			else
				printf "There is no plugin %s" "$dlpl" >$OUTPUT
				msg_output 10 60 "DELETE PLUGIN"
			fi
		else
			printf "File not deleted, go back to main menu" >$OUTPUT
			info_output 10 60 "DELETE PLUGIN"
			sleep 3
			MENU
		fi
		;;
		1)
			printf "Exit pressed, go back to main menu." >$OUTPUT
			info_output 10 60 "DELETE PLUGIN"
			sleep 3
			MENU
		;;
		255)
			printf "[ESC] key pressed, go back to main menu." >$OUTPUT
			info_output 10 60 "DELETE PLUGIN"
			sleep 3
			MENU
		;;
	esac
}

function DELETEWORLD() {
	if [ "$SERVER" = "1" ]; then
		SERVVAR1
	fi
	if [ "$SERVER" = "2" ]; then
		SERVVAR2
	fi
	if [ "$SERVER" = "3" ]; then
		SERVVAR3
	fi
	if [ "$SERVER" = "4" ]; then
		SERVVAR4
	fi
	dialog --clear --cancel-label "Exit" --ok-label "Delete" \
	--backtitle "Minecraft Server Startscript" \
	--title "DELETE WORLD" \
	--inputbox "You are going to delete a world, write worldname " 8 60 2>$OUTPUT
	respose=$?
	dlwo=$(<"${OUTPUT}")
	case $respose in
	  0)
		printf "Remove the world %s" "$dlpl?" >$OUTPUT
		yesno_output 10 60 "DELETE WORLD"
		FILEDELETE=$?
		if [ "$FILEDELETE" = 0 ]; then
			if [ -d "$DIR/plugins/$dlpl" ]; then
				rm -rf "$DIR/world/$dlwo"
				printf "The world %s is deleted" "$dlpl" >$OUTPUT
				info_output 10 60 "DELETE WORLD"
				sleep 3
				MENU
			else
				printf "There is no world %s" "$dlpl" >$OUTPUT
				msg_output 10 60 "DELETE WORLD"
			fi
		else
			printf "File not deleted, go back to main menu" >$OUTPUT
			info_output 10 60 "DELETE WORLD"
			sleep 3
			MENU
		fi
		;;
		1)
			printf "Exit pressed, go back to main menu." >$OUTPUT
			info_output 10 60 "DELETE WORLD"
			sleep 3
			MENU
		;;
		255)
			printf "[ESC] key pressed, go back to main menu." >$OUTPUT
			info_output 10 60 "DELETE WORLD"
			sleep 3
			MENU
		;;
	esac
}

function START() {
	printf "starting %s" "$DESC" >$OUTPUT
	info_output 10 60 "START $DESC"
	sleep 3
	if [ -e "$DIR" ] && [ -x "$DIR" ];then
		cd "$DIR" | exit
		if [ ! -x "$MODJAR" ]; then
			printf "%s is not executable, trying to set it \nuse the commmand \n\"chmod u+x %s\"" "$MODJAR" "$DIR" "$MODJAR" >$OUTPUT
			msg_output 10 60 "START $DESC"
		else
			screen -dmS $NAME $DAEMON $PARAMS -jar $MODJAR $PARAMSEND > /dev/null &
			PID=$!
			PID=$((PID+1))
			if [ "$?" -ne "0" ]; then
				printf "%s could not start" "$DESC" >$OUTPUT
				msg_output 10 60 "START $DESC"
			else
				echo $PID > mcserver.pid
				printf "%s is started with PID \"%s\"" "$DESC" "$PID" >$OUTPUT
				info_output 10 60 "START $DESC"
				sleep 3
			fi
		fi
	fi
	STARTF
}

function STOP() {
	printf "Stopping %s" "$DESC"
	printf
	screen -dr "$PID" -p 0 -X stuff "$(printf "save-all\r")"
	printf "30 Second Warning." >$OUTPUT
	info_output 10 60 "STOP $DESC"
	screen -dr "$PID" -p 0 -X stuff "$(printf "say Server will restart in 30s !\r")"
	sleep 20
	printf "10 Second Warning." >$OUTPUT
	info_output 10 60 "STOP $DESC"
	screen -dr "$PID" -p 0 -X stuff "$(printf "say Server will restart in 10s ! Please dissconnect \r")"
	sleep 10
	screen -dr "$PID" -p 0 -X stuff "$(printf "stop\r")"
	PID=$((PID+1))
	if ( kill -2 $PID 2> /dev/null ); then
		c=1
		while [ "$c" -le 100 ]; do
			if ( kill -0 $PID 2> /dev/null ); then
				sleep 1
			else
				break
			fi
			echo $c | dialog --gauge "Sopping Server Please wait" 10 70 0;
			c=$(($c+1))
		done
		if ( kill -0 $PID 2> /dev/null ); then
			echo "Server is not shutting down cleanly - killing?" >$OUTPUT
			yesno_output 10 60 "UPDATES $DESC"
			KILLING=$?
			if [ "$KILLING" = 0 ]; then
				kill -KILL $PID
				printf "Server stopped" >$OUTPUT
				info_output 10 60 "STOP $DESC"
				sleep 3
			fi
		else
			printf "Server stopped" >$OUTPUT
			info_output 10 60 "STOP $DESC"
			sleep 3
		fi
	fi
	rm "$DIR/mcserver.pid"
	STOPF
}

function UPDATE() {
	if ( kill -0 $(cat "$DIR/mcserver.pid") 2> /dev/null ); then
		printf "%s is running, continue?" "$DESC" >$OUTPUT
		yesno_output 10 60 "UDPATE SERVER"
		SERVEROFF=$?
		if [ "$SERVEROFF" = 0 ]; then
			wait 1
		elif [ "$SERVEROFF" = 1 ]; then
			MENU
		elif [ "$SERVEROFF" = 255 ]; then
			MENU
		fi
	fi
	BACKUP
	if [ "$SERVERUPDATE" = "true" ]; then
		SERVERUPDATE
	fi
	if [ "$PLUGINUPDATE" = "true" ]; then
		PLUGINUPDATE
	fi
	if [ "$MODUPDATE" = "true" ]; then
		MODUPDATE
	fi
	if [ "$BACKUPOLD" = "true" ]; then
		BACKUPOLD
	fi
	UPDATEF
}

function BACKUP() {
	if [ ! -d "$BKUP" ]; then
		mkdir "$BKUP"
	fi
	printf "Backing up current binaries..." >$OUTPUT
	info_output 10 60 "SERVER BACKUP"
	cd "$DIR" | exit
	tar -czf "$NAME"-"$DATE".tar.gz lib/* logs/* plugins/*.jar "$SERVERSOFTWARE".jar server.properties
	mv "$NAME"-"$DATE".tar.gz "$BKUP"
	sleep 1
	printf "Server backup done" >$OUTPUT
	info_output 10 60 "SERVER BACKUP"
	sleep 3
}

function SERVERUPDATE() {
	if [ -e "$BUILDBOX/BuildTools.jar" ]; then
		rm  -rf $BUILDBOX
		mkdir -p $BUILDBOX
	fi
	cd $BUILDBOX | exit
#https://gist.github.com/Gregsen/7822421
	WGETURL="https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar"
	wget --progress=dot "$WGETURL" 2>&1 |\
	grep "%" |\
	sed -u -e "s,\.,,g" | awk '{print $2}' | sed -u -e "s,\%,,g"  | dialog --gauge "Download Test" 10 100
	if [ -e "$BUILDBOX/BuildTools.jar" ]; then
		rm
		dialog --clear --cancel-label "Exit" --ok-label "Send" \
		--backtitle "Minecraft Server Startscript" \
		--title "MINECRAFT VERSION" \
		--inputbox "Enter the Minecraft version:" 8 60 "1.9.2" 2>$OUTPUT
		respose=$?
		DLVERSION=$(<"${OUTPUT}")
		java -jar BuildTools.jar --rev $DLVERSION
		if [ -e "$BUILDBOX/spigot-"$DLVERSION".jar" ]; then
			SERVERUP="success"
			if [ "$SERVERSOFTWARE" = "spigot" ]; then

				mv -f $BUILDBOX/spigot-"$DLVERSION".jar $OUTPUT/spigot.jar
				chmod 774 $DIR/forge.jar
			elif [ "$SERVERSOFTWARE" = "craftbukkit" ]; then

				mv -f $BUILDBOX/craftbukkit-"$DLVERSION".jar $OUTPUT/craftbukkit.jar
				chmod 774 $DIR/craftbukkit.jar
			fi
		fi
	else
	SERVERUP="fail"
	fi
}

function SPONGEUPDATE() {
#	dialog --clear --cancel-label "Exit" --ok-label "Send" \
#	--backtitle "Minecraft Server Startscript" \
#	--title "FORGE DOWNLOADLINK" \
#	--inputbox "Enter the Forge download link:" 8 60 2>$OUTPUT
#	respose=$?
#	if [ "$respose" = 0 ]; then
#		FORGEURL=$(<"${OUTPUT}")
#		cd "$DIR"
#	elif [ "$respose" = 1 ]; then
#		MENU
#	elif [ "$respose" = 255 ]; then
#		MENU
#	fi
#		until [ -e "$DIR/mods/sponge.jar" ]; do
#			for c in $(seq 1 5); do
#				echo $c | dialog --gauge "Update Server Please wait" 10 70 0;
#				sleep 1
#				c=$(($c+1))
#			done
#			if [ "$c" = 100 ]; then
#				break
#			fi
#		done

	dialog --clear --cancel-label "Exit" --ok-label "Send" \
	--backtitle "Minecraft Server Startscript" \
	--title "SPONGE DOWNLOADLINK" \
	--inputbox "Enter the Sponge download link:" 8 60 2>$OUTPUT
	respose=$?
	if [ "$respose" = 0 ]; then
		SPONGEURL=$(<"${OUTPUT}")
		cd "$DIR/mods" | exit
	elif [ "$respose" = 1 ]; then
		MENU
	elif [ "$respose" = 255 ]; then
		MENU
	fi
#https://gist.github.com/Gregsen/7822421
	wget --progress=dot "$SPONGEEURL" --output-document=sponge.jar 2>&1 |\
	grep "%" |\
	sed -u -e "s,\.,,g" | awk '{print $2}' | sed -u -e "s,\%,,g"  | dialog --gauge "DOWNLOAD SPONGE" 10 100
#https://stackoverflow.com/questions/2717303/checking-wgets-return-value-if
	wget_output=$(wget -q "$SPONGEURL")
	if [ $? -ne 0 ]; then
		SPONGEUP="success"
		rm
		java -jar forge-version-installer.jar --installServer
		until [ -e "$DIR/mods/sponge.jar" ]; do
			for c in $(seq 1 5); do
				echo $c | dialog --gauge "Update Server Please wait" 10 70 0;
				sleep 1
				c=$(($c+1))
			done
			if [ "$c" = 100 ]; then
				break
			fi
		done
		chmod 774 forge.jar
	else
	FORGEUP="fail"
	fi
}

function PLUGINUPDATE() {
	printf "coming soon" >$OUTPUT
	info_output 10 60 "PLUGINUPDATE"
	sleep 3
	PLUGINUP="fail"
}

function MODUPDATE() {
	printf "coming soon" >$OUTPUT
	info_output 10 60 "MODUPDATE"
	sleep 3
	MODUP="fail"
}

function BACKUPOLD() {
	printf "Cleaning old backups and logs" >$OUTPUT
	info_output 10 60 "UDPATE SERVER"
	find "$BKUP" -type f -mtime +"$BKUPOLDT" -delete
	find "$DIR"/logs -type f -mtime +"$BKUPOLDT" -delete
	sleep 3
}
<<OLDUPDATE
			cd �DIR && wget http://files.minecraftforge.net/maven/net/minecraftforge/forge/1.8.9-11.15.1.1722/forge-1.8.9-11.15.1.1722-installer.jar
			"counter" = 0
			until [ -e "$DIR/forge-1.8.9-11.15.1.1722-installer.jar" ]; do
				for counter in $(seq 1 5); do
					printf -n "."
					sleep 5
				done
				if $counter==5; then
					break
					FORGEUP="fail"
				fi
			done
			printf !
			if [ -e "$DIR/bukkit_update/download.ready" ]; then
				FORGEUP="success"
			else
			printf "No Bukkit Update"
			fi
		fi
		if [ "$PLUGINUPDATE" = "true" ]; then
			printf "Downloading Plugin Updates..."
			screen -dr "$NAME" -p 0 -X stuff "$(printf "uptodate plugin\r")"
			until [ "$PLUGINUP" = "success" ]; do
				if [ -e "$DIR/bukkit_update/plugins/plugin.ready" ]; then
					PLUGINUP="success"
				elif [ -e "$DIR/bukkit_update/plugins/plugin.fail" ]; then
					PLUGINUP="fail"
				fi
				for c in $(seq 1 5); do
					printf -n "."
					sleep 5
				done
				if [ "$PLUGINUP" = "success" ] || [ "$PLUGINUP" = "fail" ]; then
					break
				fi
			done
			printf !
			if [ "$FORGEUP" = "success" ] || [ "$PLUGINUP" = "success" ]; then
				printf "Copy updates in the plugin Folder? (yes\no)"
				read pcopy
				if [ "$pcopy" = "yes" ] ; then
					if [ "$FORGEUP" = "success" ] ; then
						printf "Update Bukkit..."
						cp "$DIR"/bukkit_update/craftbukkit.jar "$DIR"
						chmod 777 "$DIR"/craftbukkit.jar
					else
						printf "No Bukkit Update"
					fi
					if [ "$PLUGINUP" = "success" ]; then
						printf "Update %s plugins..." "$DESC1"
						cp -r "$DIR"/bukkit_update/plugins/*.jar "$DIR"/plugins/
						printf "done"
					else
						printf "%s have no plugin updates" "$DESC"
					fi
					RESTARTSERVER="yes"
				else
					if [ "$FORGEUP" = "success" ] ; then
					printf "%s updates are in the $DIR/bukkit_update Folder" "$DESC"
					fi
					if [ "$PLUGINUP" = "success" ] ; then
					printf "%s updates are in the %s/bukkit_update/plugins Folder" "$DESC" "$DIR"
					fi
					if [ "$FORGEUP" = "fail" ] && [ "$PLUGINUP" = "fail" ]; then
						printf "%s have no Bukkit & plugin updates" "$DESC"
					fi
				fi
			else
				printf "%s have no Bukkit & plugin updates" "$DESC"
				RESTARTSERVER="no"
			fi
		fi
		if [ "$FORGEUPDATE" = "true" ] || [ "$PLUGINUPDATE" = "true" ]; then
			printf "Clean up download folder? (yes\no)"
			read pclean
			if [ "$pclean" = "yes" ] ; then
				if [ "$FORGEUPDATE" = "true" ]; then
					rm -f "$DIR"/bukkit_update/craftbukkit.jar "$DIR"/bukkit_update/download.fail "$DIR"/bukkit_update/download.ready
				fi
				if [ "$PLUGINUPDATE" = "true" ]; then
					rm -rf "$DIR"/bukkit_update/plugins/*.jar "$DIR"/bukkit_update/plugins/*.zip "$DIR"/bukkit_update/plugins/changelog/*.html "$DIR"/bukkit_update/plugins/plugin.ready "$DIR"/bukkit_update/plugins/plugin.fail
				fi
			fi
		fi
OLDUPDATE

function CONFIG() {
	if [  ! -e "config.sh" ]; then
		printf "There is no config file on the server" >$OUTPUT
		msg_output 10 60 "CONFIG FILE"
		clear
		exit 0
	else
		source config.sh
	fi
}

function CONFIG2() {
	if [  ! -e "config.sh" ]; then
		printf "There is no config file on the server \nWant to download it?" >$OUTPUT
		yesno_output 10 60 "CONFIG FILE"
		FILEDELETE=$?
		if [ "$FILEDELETE" = 0 ]; then
			wget http://test.de
			exit 1
		else
			printf "Please download the config file." >$OUTPUT
			info_output 10 60 "DELETE PLUGIN"
			sleep 3
			exit 1
		fi
	else
		source config.sh
	fi
}

function INTRO() {
	debug_on
	if which dialog; then
		if [ "$INTROOUTRO" = true ]; then
			printf "\n%s \nToday is %s." "$GREETING" "$dat" >$OUTPUT
			info_output 10 60 "(-: Welcome to Startscript :-)"
			sleep 3
			MENU
		else
			MENU
		fi
	else
		echo -e "$GREETING \nYou havent install dialog \nUse \"apt-get install dialog\""
	fi
}

function QUIT() {
	if [ "$INTROOUTRO" = true ]; then
		printf "Exit the programm. \nBye %s have a nice day." "$USER" >$OUTPUT
		info_output 10 60 "M A I N - M E N U"
		sleep 3
		clear
		exit 1
	else
		clear
		exit 1
	fi
}

function MENU() {
	dialog --clear --cancel-label "Exit" --ok-label "Select" \
	--backtitle "Minecraft Server Startscript" \
	--title "M A I N - M E N U" \
	--menu "Move using [UP] [DOWN],[Enter] to Select" 18 50 17 \
	Start "Start the server" \
	Stop "Stop the server" \
	Restart "Restart the server" \
	Serverstatus "Serverstatus of all server" \
	Say "Say something on the server" \
	Login "Login into the server" \
	Update "Update a server" \
	Restart_all "Restart all server" \
	Delete_Plugin "Delete Plugin the server" \
	Delete_World "Delete World the server" 2>"${INPUT}"
	resposes=$?
	menuitem=$(<"${INPUT}")
	case $resposes in
		1)
			QUIT
		;;
		255)
			QUIT
		;;
		*)
		case $menuitem in
			Start) ACTION=STARTS; SELECTSERVER;;
			Stop) ACTION=STOPS; SELECTSERVER;;
			Restart) ACTION=RESTART; SELECTSERVER;;
			Serverstatus) STATUS;;
			Say) ACTION=SAY; SELECTSERVER;;
			Login) ACTION=LOGIN; SELECTSERVER;;
			Restart_all) RESTARTALL;;
			Update) ACTION=UPDATES; SELECTSERVER;;
			Delete_Plugin) ACTION=DELETEPLUGIN; SELECTSERVER;;
			Delete_WORLD) ACTION=DELETEWORLD; SELECTSERVER;;
		esac
		;;
	esac
}

function SELECTSERVER() {
	if [ "$ACTION" = DELETEPLUGIN ] && [ "$DELETEPLUGIN" = "false" ]; then
		printf "\Z1Attention! \Z0This can compromise your server \nEnable this option in your config.sh" >$OUTPUT
		msg_output 10 60 "DELETE PLUGIN"
		MENU
	fi
	if [ "$ACTION" = DELETEWORLD ] && [ "$DELETEWORLD" = "false" ]; then
		printf "\Z1Attention! \Z0This can compromise your server \nEnable this option in your config.sh" >$OUTPUT
		msg_output 10 60 "DELETE WORLD"
		MENU
	fi
	auswahl=""
	if [ "$SERVERS" -ge "1" ]; then
		auswahl[0]=1
		auswahl[1]="$DESC1"
	fi
	if [ "$SERVERS" -ge "2" ]; then
		auswahl[2]=2
		auswahl[3]="$DESC2"
	fi
	if [ "$SERVERS" -ge "3" ]; then
		auswahl[4]=3
		auswahl[5]="$DESC3"
	fi
	if [ "$SERVERS" -ge "4" ]; then
		auswahl[6]=4
		auswahl[7]="$DESC4"
	fi
	dialog --clear --cancel-label "Exit" --ok-label "Select" --backtitle "Minecraft Server Startscript" --title "SELECT A SERVER" --menu "Move using [UP] [DOWN],[Enter] to Select" 18 50 17 "${auswahl[@]}" 2>"${INPUT}"
	resposes=$?
	case $resposes in
	0)
		SERVER=$(<"${INPUT}")
		$ACTION
	;;
	1)
		echo "Exit pressed, go back to main menu." >$OUTPUT
		info_output 10 60 "SELECT A SERVER"
		sleep 3
		MENU
	;;
	255)
		echo "[ESC] key pressed, go back to main menu." >$OUTPUT
		info_output 10 60 "SELECT A SERVER"
		sleep 3
		MENU
	;;
	esac
}

function CRON() {
	COUNT=1
	if [ ! -d "$BKUP" ]; then
		mkdir "$BKUP"
	fi
	while true; do
		if [ "$COUNT" = 1 ]; then
			SERVVAR1
		elif [ "$COUNT" = 2 ]; then
			SERVVAR2
		elif [ "$COUNT" = 3 ]; then
			SERVVAR3
		elif [ "$COUNT" = 4 ]; then
			SERVVAR4
		fi
		cd "$DIR" | exit
		tar -czf "$NAME"-"$DATE".tar.gz config/* libraries/* mods/* forge.jar minecraft_server*.jar server.properties
		mv "$NAME"-"$DATE".tar.gz "$BKUP"
		if [ "$BKUPWORLD" = "true" ]; then
			if [ ! -d "$BKUP"/"$NAME" ]; then
				mkdir "$BKUP"/"$NAME"
			fi
			tar -czf "$NAME"-"$DATE".tar.gz "$WORLDNAME"/*
			mv "$NAME"-"$DATE".tar.gz "$BKUP"/"$NAME"/
		fi
		if [ "$BKUPNETHER" = "true" ]; then
			if [ ! -d "$BKUP"/"$NAME" ]; then
				mkdir "$BKUP"/"$NAME"
			fi
			tar -czf "$NAME"_nether-"$DATE".tar.gz "$WORLDNAME"_nether/*
			mv "$NAME"_nether-"$DATE".tar.gz "$BKUP"/"$NAME"/
		fi
		if [ "$CRONBACKUPOLD" = "true" ]; then
			find "$BKUP" -type f -mtime +"$BKUPOLDT" -delete
			find "$DIR"/logs -type f -mtime +"$BKUPOLDT" -delete
		fi
		if [ ! -z "$CRONCOMMAND" ]; then
			screen -dr "$NAME" -p 0 -X stuff "$(printf "say %s\n" "$CRONCOMMAND")"
		fi
		if [ "$COUNT" -ge "$SERVERS" ]; then
			break
		fi
		COUNT=$((COUNT+1))
	done
exit 1
}

case "$1" in
cron)
	CRON
;;

autor)
	RESTARTALL
;;

*)
	export NCURSES_NO_UTF8_ACS=1
	CONFIG
	INTRO
;;
esac
